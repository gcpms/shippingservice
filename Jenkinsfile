registry= "345331916214.dkr.ecr.us-east-2.amazonaws.com"
def version = getVersion()
def tag = getTag(version)

pipeline{
    agent none
    stages{
        stage("Build Image"){
            agent any
            steps{
                script{
                    sh "docker build . -t ${tag}"
                }
            }
        }
        stage("Login to Ecr"){
            agent any
            steps{
                script{
                    withAWS(region:'us-east-1',credentials:'jenkins-user') {
                        awsLogin("docker")

                    }
                }
            }
        }
        stage("Push Image to Ecr"){
            agent any
            steps{
                script{
                    withAWS(region:'us-east-1',credentials:'jenkins-user') {
                        sh "docker push ${tag}"
                        // clean up docker image
                        sh " docker image rm ${tag}"
                    }
                }
            }
        }

        stage("package and push helm chart"){
            agent any
            steps{
                script{
                    sh "ls -l"
                    dir("${WORKSPACE}/helm"){
                        sh "ls -l"
                        withEnv(['HELM_EXPERIMENTAL_OCI=1']){
                            withAWS(region:'us-east-1',credentials:'jenkins-user'){
                                // read values.yaml and update deployment.image.tag with new build tag
                                def valuesYaml=  readYaml file: "values.yaml"
                                echo "values before:"
                                sh "cat values.yaml"
                                valuesYaml.deployment.image.tag = version
                                writeYaml file: 'values.yaml', data: valuesYaml, overwrite: true
                                echo "values after:"
                                echo "values after: "+ valuesYaml

                                // read the chart.yaml and update chart.version
                                def chartYaml = readYaml file: "Chart.yaml"
                                echo "values before:"
                                sh "cat Chart.yaml"
                                currentVersion = chartYaml.version
                                newVersion = "${currentVersion}-${version}"
                                chartYaml.version= newVersion
                                writeYaml file: 'Chart.yaml', data: chartYaml, overwrite: true
                                echo "values after:"
                                sh " cat Chart.yaml"

                                // package and push 
                                awsLogin("helm registry")
                                sh "rm -rf *tgz"
                                sh " helm package ."
                                sh"helm push *.tgz oci://${registry}"
                                //cleanup worspace
                                cleanWs()
                            }
                        }
                    }
                }
            }
        }
    }
}

void awsLogin(String loginType){
    sh " aws ecr get-login-password --region us-east-1 | ${loginType} login --username AWS --password-stdin $registry"
}

void getVersion(){
    def version=env.BRANCH_NAME.replaceAll("/","-")
    return "${version}-${env.BUILD_NUMBER}"
}

void getTag(String version){
    def tag
    repo= env.JOB_NAME.replace("CI/","").replace("/feature%2Freconfigure","")
    return tag="${registry}/${repo}:${version}"
}